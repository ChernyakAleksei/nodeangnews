const express = require('express');
const Db = require('./news.json');
const Readable = require('stream').Readable;

const singleNews = express.Router();

singleNews.get('/:id', (req, res, next) => {
  const Obj = Db.news.filter(item => +item.id === +req.params.id);
  if (Obj.length === 0) {
    next();
  } else {
    const rs = new Readable;
    rs.push(JSON.stringify(Obj));
    rs.push(null);
    rs.pipe(res);
  }
});


singleNews.get('/', (req, res) => {
  const Obj = Db.news.map((item) => {
    return {
      id: item.id,
      title: item.title
    };
  });
  res.status('200').json(Obj);
});

module.exports = singleNews;
