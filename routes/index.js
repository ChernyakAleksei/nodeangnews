const express = require('express');
const path = require('path');

const listNews = express.Router();

const startPagePath = path.join(__dirname, '../views/index.html');

listNews.get('/', (req, res) => {
  res.sendFile(startPagePath);
});



module.exports = listNews;
